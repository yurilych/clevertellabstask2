package com.example.clevertektask2.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.cleverteklabstask2.RecyclerViewAdapter
import com.example.clevertektask2.R
import com.example.clevertektask2.databinding.MainFragmentBinding
import com.example.clevertektask2.model.ItemModel

class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        val list = arrayListOf<ItemModel>()
        for (i in 1..1000) list.add(ItemModel(R.drawable.item_icon, "Title $i", "Description $i"))
        val adapter = RecyclerViewAdapter(list)
        adapter.setItemClickListener(object : RecyclerViewAdapter.ItemClickListener {
            override fun onItemClick(model: ItemModel) {
                viewModel?.model = model
                nextFragmentCall()
            }
        })
        binding.recyclerView.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { viewModel = ViewModelProvider(it).get(MainViewModel::class.java) }
    }

    private fun nextFragmentCall() {
        fragmentManager?.beginTransaction()?.replace(R.id.container, ItemFragment.newInstance())
            ?.addToBackStack("TAG")?.commit()
    }

    companion object {
        fun newInstance() = MainFragment()
    }
}