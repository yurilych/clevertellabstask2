package com.example.cleverteklabstask2

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.clevertektask2.databinding.ItemBinding
import com.example.clevertektask2.model.ItemModel
import java.lang.NullPointerException

class RecyclerViewAdapter(
    private val list: List<ItemModel>
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    private var listener:ItemClickListener?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.icon.setImageResource(list[position].icon)
       holder.title.text = list[position].title
       holder.description.text = list[position].description
       holder.itemView.setOnClickListener{listener?.onItemClick(list[position])}
    }

    override fun getItemCount(): Int = list.size

    inner class ViewHolder(binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) {
        var icon = binding.itemIcon
        var title = binding.itemTitle
        var description = binding.itemDescription
    }

    fun setItemClickListener(listener: ItemClickListener) {
        this.listener = listener
    }
    interface ItemClickListener {
        fun onItemClick (model: ItemModel)
    }
}

class ItemBinding {

}
