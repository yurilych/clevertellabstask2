package com.example.clevertektask2.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.clevertektask2.databinding.FragmentItemBinding
import android.R
import androidx.appcompat.widget.Toolbar


class ItemFragment : Fragment() {
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: FragmentItemBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentItemBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { viewModel = ViewModelProvider(it).get(MainViewModel::class.java) }

        binding.fragmentItemTitle.text = viewModel.model?.title
        binding.fragmentItemDescription.text =  viewModel.model?.description

        binding.closeButton.setOnClickListener{activity?.finish()}
    }

    companion object {
        fun newInstance() = ItemFragment()
    }
}